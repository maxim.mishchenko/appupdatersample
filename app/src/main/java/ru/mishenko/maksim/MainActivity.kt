package ru.mishenko.maksim

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.github.javiersantos.appupdater.AppUpdater
import com.github.javiersantos.appupdater.AppUpdaterUtils
import com.github.javiersantos.appupdater.enums.AppUpdaterError
import com.github.javiersantos.appupdater.enums.UpdateFrom
import com.github.javiersantos.appupdater.objects.Update
import ru.mishenko.maksim.ui.theme.AppUpdaterSampleTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppUpdaterSampleTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainContent()
                }
            }
        }
    }
}

@Composable
fun MainContent() {
    val context = LocalContext.current
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "version code: ${BuildConfig.VERSION_CODE}")
        Text(text = "version name: ${BuildConfig.VERSION_NAME}")
        Button(onClick = { update(context) }) {
            Text("Update")
        }
    }
}

fun update(context: Context) {
    AppUpdaterUtils(context)
        .setUpdateFrom(UpdateFrom.JSON)
        .setUpdateJSON("https://raw.githubusercontent.com/UserNameMax/AppUpdaterSample/master/updateRes/update.json")
        .withListener(object : AppUpdaterUtils.UpdateListener {
            override fun onSuccess(update: Update, isUpdateAvailable: Boolean?) {

                Log.d("Latest Version", update.latestVersion)
                Log.d("Latest Version Code", update.latestVersionCode.toString())
                Log.d("Release notes", update.releaseNotes)
                Log.d("URL", update.urlToDownload.toString())
                Log.d("Is update available?", java.lang.Boolean.toString(isUpdateAvailable!!))
            }

            override fun onFailed(error: AppUpdaterError) {
                Log.d("AppUpdater Error", error.name)
            }
        })
        .start()
}